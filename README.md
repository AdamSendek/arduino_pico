# Arduino_Pico
Last update v1: 17-06-2018  
Last update v2: 2021  

## Description
Project PCB arduino pico based on arduino Leonardo  
Size:  
v1 - 25.50 x 26.25mm  
v2 - 24.38 x 25.90mm  

## Hardware
* 1x Atmega32u4 TQFP-44
* 1x Ferrite Bead 0603
* 1x Schottky Diode MiniMELF(SOD80) (1 to 5v or 2 to 3v3)
* 1x Grean LED 0603
* 1x Red LED 0603
* Capacitors 0402:  
2x 22pF  
1x 1uF  
2x 100nF  
2x 10uF
* Micoro USB
* Mosfet-P GSD (PMV48XP)
* Rezistors 0402:  
2x 22 ohm  
1x 10k ohm  
1x 100k ochm  
1x 620 ohm  
1x 390 ohm  
2x 0 ohm (only 5v)
* 1x Switch B3U-3000P(M)-B
* 1x LM1117 (5v or 3v3)
* 1x Cristal 3225-4pin 3.2x2.5mm
* GoldPin 2.54mm:  
2x 1x08  
1x 1x10  
1x 2x03  

## Software
Arduino IDE + USBasp (or difrent programmer)
to load bootloader

## License
Creative Commons Attribution-NonCommercial-ShareAlike 4.0

## Photos
![](/Arduino_Pico_PCB_v1.JPG)
  
![](/Arduino_Pico_v1.JPG)
